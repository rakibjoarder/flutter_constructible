class FileModel {
  String fileName;
  String dateCreated;
  String fileUrl;
  String projectID;

  FileModel({this.fileName, this.dateCreated, this.fileUrl, this.projectID});

  FileModel.fromJson(Map<String, dynamic> json) {
    fileName = json['fileName'];
    dateCreated = json['dateCreated'];
    fileUrl = json['fileUrl'];
    projectID = json['projectID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fileName'] = this.fileName;
    data['dateCreated'] = this.dateCreated;
    data['fileUrl'] = this.fileUrl;
    data['projectID'] = this.projectID;
    return data;
  }
}
