class ProjectModel {
  String dateCreated;
  String title;
  bool enabled;
  String uid;

  ProjectModel({this.dateCreated, this.title, this.enabled, this.uid});

  ProjectModel.withOutUid({this.dateCreated, this.title, this.enabled});

  ProjectModel.fromJson(Map<String, dynamic> json) {
    dateCreated = json['dateCreated'];
    title = json['title'];
    enabled = json['enabled'];
    if (json['uid'] != null) {
      uid = json['uid'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dateCreated'] = this.dateCreated;
    data['title'] = this.title;
    data['enabled'] = this.enabled;
    if (this.uid != null) {
      data['uid'] = this.uid;
    }
    return data;
  }
}
