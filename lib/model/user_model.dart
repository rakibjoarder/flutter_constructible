class UserModel {
  String name;
  String email;
  String date;
  String uid;

  UserModel({this.name, this.email, this.date, this.uid});

  UserModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    date = json['date'];
    uid = json['uid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['date'] = this.date;
    data['email'] = this.email;
    data['uid'] = this.uid;
    return data;
  }
}
