import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_constructible/helper/authentication.dart';
import 'package:flutter_constructible/utils/screen_aware_size.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  Auth auth = new Auth();

  final _formKey = new GlobalKey<FormState>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  String _email;
  String _password;
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  AnimationController _animationController;
  Animation _animation;
  bool _isLoading;

  @override
  void initState() {
    _isLoading = false;
    super.initState();
    _animationController = new AnimationController(
        vsync: this, duration: Duration(microseconds: 500));
    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_animationController);
    _animationController.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }


  //it will show when procesing to login
  Widget _showCircularProgressIndicator() {
    if (_isLoading) {
      return Container(
        margin: EdgeInsets.all(10),
        width: 25,
        height: 25,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      );
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  //for showing response we will from the firebase
  void _showSnackBar(String msg) {
    SnackBar snackBar = new SnackBar(
      content: new Text(
        msg,
        style: TextStyle(color: Colors.white),
      ),
      duration: new Duration(seconds: 2),
      backgroundColor: Colors.black,
      action: SnackBarAction(
          label: "Undo", textColor: Colors.white, onPressed: () {}),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.teal, Colors.tealAccent])),
          child: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              Align(
                child: _showCircularProgressIndicator(),
                alignment: Alignment.bottomCenter,
              ),
              Positioned(
                  top: MediaQuery.of(context).size.height * 0.12,
                  child: _showLogo()),
              Positioned(
                  width: MediaQuery.of(context).size.width - 30,
                  top: MediaQuery.of(context).size.height * 0.35,
                  child: _showBody()),
            ],
          ),
        ),
      ),
    );
  }

  // Check if form is valid before perform login or signup
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      FocusScope.of(context).requestFocus(new FocusNode()); //keyboard close
      form.save();
      return true;
    }
    return false;
  }

  // Perform login
  void _validateAndSubmit() async {
    if (_validateAndSave()) {
      setState(() {
        _isLoading = true;
      });
      FirebaseUser currentUser ;
      try {
        currentUser = await auth.signIn(_email, _password);
        if (currentUser != null) {}
      } catch (e) {
        if (e.toString().contains('PlatformException')) {
          print('Error: $e');
          setState(() {
            _isLoading = false;
            if (e.toString().contains('ERROR_USER_NOT_FOUND')) {
              _showSnackBar(
                  'There is no user record corresponding to this identifier. The user may have been deleted');
            } else if (e.toString().contains('ERROR_WRONG_PASSWORD')) {
              _showSnackBar(
                  'The password is invalid or the user does not have a password');
            } else {
              _showSnackBar(e.toString());
            }
          });
        }
      }
    }
  }


  //White Card
  Widget _showBody() {
    return Container(
        child: new Form(
      key: _formKey,
      child: Card(
        elevation: 20,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: screenAwareSize(20, context)),
            _showEmailInput(),
            _showPasswordInput(),
            SizedBox(height: screenAwareSize(20, context)),
            _showPrimaryButton(context),
            SizedBox(height: screenAwareSize(10, context)),
            _showSecondaryButton(),
            SizedBox(
              width: screenAwareSize(200, context),
              height: screenAwareSize(40, context),
              child: RaisedButton(
                elevation: 8.0,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
                child: Row(children: <Widget>[ Expanded(flex:4,child: Image.asset('assets/googleLogo.png',width: 20,height: 20,),),Expanded(flex: 6,child: Text('Google', style: new TextStyle(
                    fontSize: screenAwareSize(18, context), color: Colors.black)),)],),
                onPressed: () async {
                 var result = await auth.sigInWithGoogle();
                 //0 if signindialog is close,1 - if already have an account in email/password ,2-
                 if(result == 1){
                   _showExistingAccountAlert();
                 }
                },
              ),

            ),
            SizedBox(height: screenAwareSize(20, context)),

          ],
        ),
      ),
    ));
  }

  //accounts alert
  void _showExistingAccountAlert() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.all(Radius.circular(12.0))),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: screenAwareSize(10, context)),
                Text('Account Already Exist',
                  style: TextStyle(color: Colors.black87,
                      fontSize: screenAwareSize(26, context)),
                ),
                SizedBox(height: screenAwareSize(10, context)),
                Padding(
                  padding: EdgeInsets.all(screenAwareSize(8.0, context)),
                  child: Text('You already have an account with this email using Email/Password auth provider.',
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: screenAwareSize(16, context)),
                    textAlign: TextAlign.center,
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('CLOSE',
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.teal,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    elevation: 6.0,
                  ),
                )
              ],
            ),
            contentPadding: EdgeInsets.all(10),
            titlePadding: EdgeInsets.all(20),
          );
        });
  }



  //app Logo
  Widget _showLogo() {
    return new Hero(
        tag: 'hero',
        child: AnimatedBuilder(
          animation: _animation,
          builder: (BuildContext context, Widget widger) {
            return FlutterLogo(
              size: _animation.value * screenAwareSize(90, context),
            );
          },
        ));
  }

  //user email
  Widget _showEmailInput() {
    return Padding(
      padding: const EdgeInsets.only(left: 14.0, right: 14.0, bottom: 10),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            contentPadding: EdgeInsets.all(0.0),
            filled: true,
            labelText: 'Email',
            fillColor: Colors.white,
            prefixIcon: new Icon(
              Icons.mail,
            )),
        validator: (value) {
          if (value.isEmpty) {
            return 'Email can\'t be empty';
          } else if (!_emailRegExp.hasMatch(value)) {
            return 'Invalid Email';
          }
        },
        onSaved: (value) => _email = value,
      ),
    );
  }

  //user Password
  Widget _showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.only(left: 14.0, right: 14),
      child: TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
            filled: true,
            labelText: 'Password',
            contentPadding: EdgeInsets.all(0.0),
            fillColor: Colors.white,
            prefixIcon: new Icon(
              Icons.lock,
            )),
        validator: (value) {
          if (value.isEmpty) {
            return 'Password can\'t be empty';
          } else if (value.length < 6) {
            return 'Password can\'t be less than 6 character';
          }
        },
        onSaved: (value) => _password = value,
      ),
    );
  }

  //to navigate to the sign up page
  Widget _showSecondaryButton() {
    return Align(
      child: FlatButton(
        child: new Text('Create an account',
            style: new TextStyle(
                fontSize: screenAwareSize(18, context),
                fontWeight: FontWeight.w600,
                color: Colors.black)),
        onPressed: () {
          Navigator.of(context).pushNamed('/signup');
        },
      ),
      alignment: Alignment.bottomCenter,
    );
  }

  //login button
  Widget _showPrimaryButton(context) {
    return SizedBox(
      width: screenAwareSize(200, context),
      height: screenAwareSize(40, context),
      child: RaisedButton(
        elevation: 8.0,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(20.0)),
        color: _isLoading == false ? Theme.of(context).primaryColor : Theme.of(context).buttonColor,
        child: new Text('Login',
            style: new TextStyle(
                fontSize: screenAwareSize(18, context), color: Colors.white)),
        onPressed:_isLoading == false ? _validateAndSubmit : null,
      ),
    );
  }
}
