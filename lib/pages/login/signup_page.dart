import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_constructible/model/user_model.dart';
import 'package:flutter_constructible/helper/authentication.dart';
import 'package:flutter_constructible/helper/cloud_firestore_helper.dart';
import 'package:flutter_constructible/utils/screen_aware_size.dart';


class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage>   with SingleTickerProviderStateMixin {
  Auth auth = new Auth();

  final _formKey = new GlobalKey<FormState>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  FireCloudStoreHelper cloudhelper = new FireCloudStoreHelper();
  String _name;
  String _email;
  String _password;
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );
  // Initial form is login form
  bool _isLoading;


   //while creating account it will show
  Widget _showCircularProgressIndicator() {
    if (_isLoading) {
      return Container(
        margin: EdgeInsets.all(10),
        width: 25,
        height: 25,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      );
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  //for showing response we get from firebase auth
  void _showSnackBar(String msg) {
    SnackBar snackBar = new SnackBar(
      content: new Text(
        msg,
        style: TextStyle(color: Colors.white),
      ),
      duration: new Duration(seconds: 2),
      backgroundColor: Colors.black,
      action: SnackBarAction(
          label: "Undo", textColor: Colors.white, onPressed: () {}),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              gradient:
              LinearGradient(colors: [Colors.teal, Colors.tealAccent])),
          child:  Container(
            child: Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Align(
                  child: _showCircularProgressIndicator(),
                  alignment: Alignment.bottomCenter,
                ),
                Positioned(
                    width: MediaQuery.of(context).size.width - 30,
                    top: MediaQuery.of(context).size.height * 0.20,
                    child: _showBody()),
              ],
            ),
          ),
        ),
      ),

    );
  }

  // Check if form is valid before perform login or signup
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      FocusScope.of(context).requestFocus(new FocusNode()); //keyboard close
      form.save();
      return true;
    }
    return false;
  }

  // Perform login or signup
  void _validateAndSubmit() async {
    if (_validateAndSave()) {
      setState(() {
        _isLoading = true;
      });
      FirebaseUser user;
      try {
        user = await auth.signUp(_email, _password);
        auth.sendEmailVerification();
        if(user != null){
          auth.getCurrentUser().then((currentUser) async {
            if(currentUser != null) {
              var _date = new DateTime.now().toString();
             var result = await cloudhelper.storeNewUser(new UserModel(name: _name,email: _email,date: _date ,uid: currentUser.uid));
             print(result);
             if(result){
               Navigator.of(context).pop();
             }
            }
          });

        }
        setState(() {
          _isLoading = false;
        });
      } catch (e) {
        if (e.toString().contains('PlatformException')) {
          print('Error: $e');
          setState(() {
            _isLoading = false;
            if (e.toString().contains('ERROR_EMAIL_ALREADY_IN_USE')) {
              _showSnackBar(
                  'The email address is already in use by another account.');
            } else if (e.toString().contains('ERROR_INVALID_EMAIL')) {
              _showSnackBar(
                  'The email address is badly formatted.');
            } else {
              _showSnackBar(e.toString());
            }
          });
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _isLoading = false;
  }

  @override
  void dispose() {
    super.dispose();
  }



  Widget _showBody() {
    return Container(
        child: new Form(
          key: _formKey,
          child: Card(
            elevation: 20,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                 Padding(child: Text('SIGN UP',style: TextStyle(fontSize: screenAwareSize(30, context)),),padding: EdgeInsets.all(20),),
                _showNameInput(),
                _showEmailInput(),
                _showPasswordInput(),
                SizedBox(height: screenAwareSize(20, context)),
                _showPrimaryButton(context),
                SizedBox(height: screenAwareSize(20, context)),
                _showSecondaryButton(),
              ],
            ),
          ),
        ));
  }

  //user email
  Widget _showEmailInput() {
    return  Padding(
      padding: const EdgeInsets.only(left: 14.0,right: 14.0,bottom: 10),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(20),
            ),
            contentPadding: EdgeInsets.all(0.0),
            filled: true,
            labelText: 'Email',
            fillColor: Colors.white,
            prefixIcon: new Icon(
              Icons.mail,
            )),
        validator: (value) {
          if (value.isEmpty) {
            return 'Email can\'t be empty';
          } else if (!_emailRegExp.hasMatch(value)) {
            return 'Invalid Email';
          }
        },
        onSaved: (value) => _email = value,
      ),
    );
  }

// user name
  Widget _showNameInput() {
    return  Padding(
      padding: const EdgeInsets.only(left: 14.0,right: 14.0,bottom: 10),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: new InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(20),
            ),
            contentPadding: EdgeInsets.all(0.0),
            filled: true,
            labelText: 'Name',
            fillColor: Colors.white,
            prefixIcon: new Icon(
              Icons.person,
            )),
        validator: (value) {
          if (value.isEmpty) {
            return 'Name can\'t be empty';
          }
        },
        onSaved: (value) => _name = value,
      ),
    );
  }
//  user password
  Widget _showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.only(left: 14.0,right: 14),
      child: TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
            filled: true,
            labelText: 'Password',
            contentPadding: EdgeInsets.all(0.0),
            fillColor: Colors.white,
            prefixIcon: new Icon(
              Icons.lock,
            )),
        validator: (value) {
          if (value.isEmpty) {
            return 'Password can\'t be empty';
          } else if (value.length < 6) {
            return 'Password can\'t be less than 6 character';
          }
        },
        onSaved: (value) => _password = value,
      ),
    );
  }

  //for navigate to login page
  Widget _showSecondaryButton() {
    return Align(
      child: FlatButton(
        child: new Text('Already have an account? Sign in',
            style: new TextStyle(
                fontSize: screenAwareSize(16, context),
                fontWeight: FontWeight.w500,
                color: Colors.black)),
        onPressed: (){
          Navigator.of(context).pop();
        },
      ),
      alignment: Alignment.bottomCenter,
    );
  }

  // submit button
  Widget _showPrimaryButton(context) {
    return SizedBox(
      width: screenAwareSize(200, context),
      height: screenAwareSize(40, context),
      child: RaisedButton(
        elevation: 8.0,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(20.0)),
        color:  _isLoading ==  false? Theme.of(context).primaryColor : Theme.of(context).primaryColorLight,
        child:  new Text('CREATE ACCOUNT',
            style: new TextStyle(fontSize: screenAwareSize(16, context), color: Colors.white)),
        onPressed: _isLoading ==  false? _validateAndSubmit : (){},
      ),
    );
  }
}
