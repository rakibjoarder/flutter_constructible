import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_constructible/pages/projects/project_list.dart';
import 'login/login_page.dart';

class PageRoutes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _handleCurrentScreen();
  }
}
//handle login state
Widget _handleCurrentScreen() {
  return StreamBuilder<FirebaseUser>(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, snapshot) {
        print(snapshot.data);
        if (snapshot.connectionState == ConnectionState.waiting) {
          return new Loader();
        } else {
          if (snapshot.hasData) {
            return new ProjectList(user: snapshot.data,);
          } else {
            return LoginPage();
          }
        }
      });
}


//loader
class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CircularProgressIndicator(),
    );
  }
}