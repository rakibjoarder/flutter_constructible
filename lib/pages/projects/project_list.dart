import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_constructible/helper/authentication.dart';
import 'package:flutter_constructible/pages/files/file_list.dart';
import 'package:flutter_constructible/utils/screen_aware_size.dart';
import 'package:flutter_constructible/helper/cloud_firestore_helper.dart';
import 'package:flutter_constructible/pages/projects/add_project.dart';
import 'package:flutter_constructible/pages/projects/edit_project.dart';


class ProjectList extends StatefulWidget {
  final FirebaseUser user;
  ProjectList({this.user});
  @override
  _ProjectListState createState() => _ProjectListState();
}

class _ProjectListState extends State<ProjectList> {

  FireCloudStoreHelper  _cloudStoreHelper =new FireCloudStoreHelper();
  FirebaseUser firebaseUser;
  Auth auth = new Auth();
  bool isLoggedIn = false;


  //login state check
  _isLoggedIn() async {
    auth.getCurrentUser().then((user) {
      if (user != null) {
        setState(() {
          isLoggedIn = true;
        });
      } else {
        setState(() {
          isLoggedIn = false;
        });
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isLoggedIn();
    auth.getCurrentUser().then((user) {
      setState(() {
        firebaseUser = user;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Projects'),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            appLogo(context),
            drawerItem(context, 'Home', Icons.home,'home'),
            isLoggedIn ? drawerItem(context, 'Log Out', Icons.exit_to_app,'logout') : Container(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: (){
         Navigator.of(context).push(MaterialPageRoute(builder: (context){
           return AddProject(user : widget.user);
         }));
      }),
      body: Container(
        child: StreamBuilder(
          stream: Firestore.instance.collection('projects').where(
              'uid', isEqualTo: widget.user.uid).snapshots(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            }
            return projectListView(snapshot);
          },
        ),
      ),

    );
  }

  ListView projectListView(AsyncSnapshot snapshot) {
    return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, ind) {
                return Card(
                  elevation: 4,
                  child: ListTile(
                    contentPadding: EdgeInsets.all(10),
                    title: Text(snapshot.data.documents[ind]['title'],
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: screenAwareSize(22, context)
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.only(top : 4.0),
                      child: Text(snapshot.data.documents[ind]['dateCreated'],
                        style: TextStyle(color: Colors.black,
                            fontSize: screenAwareSize(15, context),
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    leading: CircleAvatar(
                      child: Text(snapshot.data.documents[ind]['title'].toString().toUpperCase().substring(0,1),style: TextStyle(color: Colors.white,fontSize: screenAwareSize(22,context),fontWeight: FontWeight.bold),),
                      backgroundColor:snapshot.data.documents[ind]['enabled'] ? Colors.green : Colors.red,
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.edit,color: Colors.orange,),
                          onPressed: (){
                            Navigator.of(context).push(MaterialPageRoute(builder: (context)=> EditProject(projectItem: snapshot.data.documents[ind],)));
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.delete,color: Colors.red,),
                          onPressed: (){
                            _cloudStoreHelper.deleteProject(snapshot.data.documents[ind].documentID);
                          },
                        ),
                       IconButton(
                         icon: Icon(Icons.insert_drive_file,color: Colors.blue,),
                         onPressed: (){
                           Navigator.of(context).push(MaterialPageRoute(
                               builder: (context) =>
                                   FileList(projectItem: snapshot.data
                                       .documents[ind],)));
                         },
                       )
                      ],
                    ),
                  ),
                );
              });
  }

  //appLogo
  Container appLogo(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      height: screenAwareSize(150, context),
      child: Center(
        child: FlutterLogo(size: screenAwareSize(80, context),),
      ),
    );
  }

  //drawer Items
  ListTile drawerItem(BuildContext context, String title, IconData icon,[String route]) {
    return new ListTile(
      title: Text(title),
      leading: Icon(icon),
      onTap: () {
        if (route == 'logout') {
          Navigator.of(context).pop();
          FirebaseAuth.instance.signOut();
        } else if (route == 'Home') {
          Navigator.of(context).pop();
        } else {
          Navigator.of(context).pop();
        }
      },
    );
  }

}

