import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_constructible/helper/cloud_firestore_helper.dart';
import 'package:flutter_constructible/model/project_model.dart';
import 'package:flutter_constructible/utils/screen_aware_size.dart';
import 'package:intl/intl.dart';

class AddProject extends StatefulWidget {
  final FirebaseUser user;
  AddProject({this.user});
  @override
  _AddProjectState createState() => _AddProjectState();
}

class _AddProjectState extends State<AddProject> {

  final _formKey = new GlobalKey<FormState>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  FireCloudStoreHelper cloudhelper = new FireCloudStoreHelper();
  ProjectModel model = new ProjectModel();
  String _title;
  bool isenabled;
  bool _isLoading =false;
  bool isSwitched = false;
  var switchText = 'Disabled';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Add Project'),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.teal, Colors.tealAccent])),
          child: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              Align(
                child: _showCircularProgressIndicator(),
                alignment: Alignment.bottomCenter,
              ),
              Positioned(
                  width: MediaQuery.of(context).size.width - 30,
                  top: MediaQuery.of(context).size.height * 0.25,
                  child: _showBody()),
            ],
          ),
        ),
      ),
    );
  }

  // Check if form is valid
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      FocusScope.of(context).requestFocus(new FocusNode()); //keyboard close
      form.save();
      return true;
    }
    return false;
  }


  //Project Title
  Widget _showTitleInput() {
    return  Padding(
      padding: const EdgeInsets.only(left: 14.0,right: 14.0,bottom: 10),
      child: TextFormField(
        maxLines: 1,
        autofocus: false,
        decoration: new InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(20),
            ),
            contentPadding: EdgeInsets.all(0.0),
            filled: true,
            labelText: 'Title',
            fillColor: Colors.white,
            prefixIcon: new Icon(
              Icons.mail,
            )),
        validator: (value) {
          if (value.isEmpty) {
            return 'Title can\'t be empty';
          }
        },
        onSaved: (value) => _title = value,
      ),
    );
  }

  // validate and save project on firebase
  void _validateAndSubmit() async {
    if (_validateAndSave()) {
      setState(() {
        _isLoading = true;
      });
      var _date = new DateFormat.yMd().add_jm().format(new DateTime.now());
//      model = ProjectModel(title:  _title,enabled: isSwitched,dateCreated:_date,uid: widget.user.uid);
      model = ProjectModel(title: _title,enabled: isSwitched, dateCreated: _date, uid:  widget.user.uid);
      var result = await cloudhelper.addProject(model);
      if(result){
        Navigator.of(context).pop();
      }else{
        setState(() {
          _isLoading = false;
        });
      }

    }
  }

  //White Card
  Widget _showBody() {
    return Container(
        child: new Form(
          key: _formKey,
          child: Card(
            elevation: 20,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: screenAwareSize(20, context)),
                _showTitleInput(),
                SizedBox(height: screenAwareSize(10, context)),
                ListTile(
                  contentPadding: EdgeInsets.only(left: 30,right: 30),
                  title: Text(switchText),
                  trailing:  Switch(
                    value: isSwitched,
                    onChanged: (value) {
                      setState(() {
                        isSwitched = value;
                        switchText = value ? 'Enabled' : 'Disabled';
                      });
                    },
                    activeTrackColor: Colors.lightGreenAccent,
                    activeColor: Colors.green,
                    inactiveThumbColor: Colors.red,
                  ),
                ),
                _showPrimaryButton(context),
                SizedBox(height: screenAwareSize(20, context)),
              ],
            ),
          ),
        ));
  }

//for showing while uploading project
  Widget _showCircularProgressIndicator() {
    if (_isLoading) {
      return Container(
        margin: EdgeInsets.all(10),
        width: 25,
        height: 25,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      );
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }


  // submit button
  Widget _showPrimaryButton(context) {
    return SizedBox(
      width: screenAwareSize(200, context),
      height: screenAwareSize(40, context),
      child: RaisedButton(
        elevation: 8.0,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(20.0)),
        color:  _isLoading ==  false? Theme.of(context).primaryColor : Theme.of(context).primaryColorLight,
        child:  new Text('ADD',
            style: new TextStyle(fontSize: screenAwareSize(16, context), color: Colors.white)),
        onPressed: _isLoading ==  false? _validateAndSubmit : (){},
      ),
    );
  }


}
