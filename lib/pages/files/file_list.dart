import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_constructible/helper/cloud_firestore_helper.dart';
import 'package:flutter_constructible/pages/files/add_file.dart';
import 'package:flutter_constructible/utils/screen_aware_size.dart';

import 'edit_file.dart';

class FileList extends StatefulWidget {
  final projectItem;
  FileList({this.projectItem});
  @override
  _FileListState createState() => _FileListState();
}

class _FileListState extends State<FileList> {
  FireCloudStoreHelper  _cloudStoreHelper =new FireCloudStoreHelper();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=> AddFile(projectItem: widget.projectItem,)));
          }),
      body: Container(
        child: StreamBuilder(
          stream: Firestore.instance.collection('files').where('projectID', isEqualTo: widget.projectItem.documentID).snapshots(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            }
            return listView(snapshot);
          },
        ),
      ),

    );
  }


  //File List View
  ListView listView(AsyncSnapshot snapshot) {
    return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, ind) {
                return Card(
                  elevation: 4,
                  child: ListTile(
                    contentPadding: EdgeInsets.all(10),
                    title: Text(snapshot.data.documents[ind]['fileName'],
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: screenAwareSize(22, context)
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.only(top : 4.0),
                      child: Text(snapshot.data.documents[ind]['dateCreated'],
                        style: TextStyle(color: Colors.black,
                          fontSize: screenAwareSize(15, context),
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    leading: CircleAvatar(
                      child: Text(snapshot.data.documents[ind]['fileName'].toString().toUpperCase().substring(0,1),style: TextStyle(color: Colors.white,fontSize: screenAwareSize(22,context),fontWeight: FontWeight.bold),),
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.edit,color: Colors.orange,),
                          onPressed: (){
                            print(snapshot.data.documents[ind]['fileName']);
                           Navigator.of(context).push(MaterialPageRoute(builder: (context)=> EditFile(fileItem : snapshot.data.documents[ind],)));
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.delete,color: Colors.red,),
                          onPressed: (){
                            _cloudStoreHelper.deleteFile(snapshot.data.documents[ind].documentID);
                          },
                        ),
                      ],
                    ),
                  ),
                );
              });
  }

  //appbar
  AppBar appBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.teal,
      elevation: 0.0,
      leading: new IconButton(
        icon: new Icon(
          Icons.arrow_back_ios,
        ),
        color: Colors.white,
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      title: new Text('Files - '+widget.projectItem['title'],maxLines: 1,overflow: TextOverflow.ellipsis,),
    );
  }
}
