import 'dart:io';
import 'dart:math';
import 'package:connectivity/connectivity.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_constructible/helper/authentication.dart';
import 'package:flutter_constructible/helper/cloud_firestore_helper.dart';
import 'package:flutter_constructible/model/file_model.dart';
import 'package:flutter_constructible/utils/screen_aware_size.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';

class EditFile extends StatefulWidget {
  final fileItem;
  EditFile({this.fileItem});
  @override
  _EditFileState createState() => _EditFileState();
}

class _EditFileState extends State<EditFile> {
  var _formState = new GlobalKey<FormState>();
  Auth auth = new Auth();
  File file;
  String _fileName,extension ,finalFileName;
  String fileUrl ;
  bool uploadingStatus= false;
  bool fileRequired= false;
  FireCloudStoreHelper cloudStoreHelper = new FireCloudStoreHelper();
  TextEditingController _controller = new TextEditingController();


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller?.dispose();
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fileUrl = widget.fileItem['fileUrl'];
    _controller.text = widget.fileItem['fileName'];
  }




  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        uploadingStatus == true ? null : Navigator.of(context).pop();
      },
      child: Scaffold(
        appBar: appBar(context),
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(gradient: LinearGradient(
                  colors: [Colors.teal, Colors.tealAccent])),
              child: Center(
                child: SingleChildScrollView(
                  child: Form(
                      key: _formState,
                      child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          elevation: 4,
                          child: Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(height: screenAwareSize(10, context),),
                                uploadField(),
                                fileRequired == true ? showWarningIfFileisEmpty() : SizedBox(),
                                SizedBox(height: screenAwareSize(10, context),),
                                nameTextForm(context),
                                SizedBox(height: screenAwareSize(15, context),),
                                submitButton(context)
                              ],
                            ),
                          ))),
                ),
              ),
            ),
            uploadingStatus ? updatingDialog(context) : SizedBox(),
          ],
        ),
      ),
    );
  }


  //if file is empty
  Padding showWarningIfFileisEmpty() {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0),
      child: Text('Please Upload  file', style: TextStyle(color: Colors.red),),
    );
  }


  //modifiy data on firebase
  void _submitForm() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    var connectionStatus = await checkInternetConnection();
    if (connectionStatus == false) {
      _showInternetAlertDialouge();
      return;
    }

    print(fileUrl);
    if (fileUrl == null) {
      setState(() {
        fileRequired = true;
      });
      Future.delayed(Duration(seconds: 3), () {
        setState(() {
          fileRequired = false;
        });
      });
    }

    if (_formState.currentState.validate() && fileUrl != null) {
      setState(() {
        uploadingStatus = true;
      });
      _formState.currentState.save();
      var _date = new DateFormat.yMd().add_jm().format(new DateTime.now());
      await cloudStoreHelper.updateFile(FileModel(fileName: finalFileName,
          fileUrl: fileUrl,
          dateCreated: _date,
          projectID: widget.fileItem['projectID']), widget.fileItem.documentID);

      setState(() {
        uploadingStatus = false;
      });
      Navigator.of(context).pop();
    }
  }


  //submit Button
  RaisedButton submitButton(BuildContext context) {
    return RaisedButton(
      elevation: 10,
      padding: EdgeInsets.only(
          right: screenAwareSize(50, context),
          left: screenAwareSize(50, context)),
      child: Text('Save', style: TextStyle(
          color: Colors.white, fontSize: screenAwareSize(20, context)),),
      color: Theme
          .of(context)
          .primaryColor,
      onPressed: uploadingStatus == false? _submitForm : null,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(screenAwareSize(20, context))),
    );
  }

  //pick file from filemanager
  Future getFile() async {
    var connectionStatus = await checkInternetConnection();
    if (connectionStatus == false) {
      _showInternetAlertDialouge();
      return;
    }

    try {
      file = await FilePicker.getFile(type: FileType.ANY);
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    }

    if (!mounted) return;

    setState(() {
      uploadingStatus = true;
    });
    if (file != null) {
      _fileName = file.path .split('/').last;
      extension = _fileName.split('.').last;
      _fileName = _fileName.split('.').first;
      await uploadFile(file, _fileName, extension);
    }
  }

  //uploading file on firebase storage.
  uploadFile(file,_fileName,extension) async{
    var randomNo1 = Random().nextInt(10000).toString();
    var randomNo2 = Random().nextInt(10000).toString();
    var randomNo3 = Random().nextInt(10000).toString();

    finalFileName = _fileName +'-'+randomNo1.toString()+'-'+randomNo2.toString()+'-'+randomNo3.toString() +'.'+extension;
    _controller.text = finalFileName;
    final StorageReference firebaseStorageRef =FirebaseStorage.instance.ref().child(finalFileName);

    final StorageUploadTask task = firebaseStorageRef.putFile(file);
    task.onComplete.then((value) async {
      var getDownloadURL = await value.ref.getDownloadURL();
      setState(() {
        fileUrl = getDownloadURL ;
        uploadingStatus = false;
      });
    });
  }


  //appbar
  AppBar appBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.teal,
      leading: new IconButton(
        icon: new Icon(
          Icons.arrow_back_ios,
        ),
        color: Colors.white,
        onPressed: () {
          uploadingStatus == true ? null :  Navigator.of(context).pop();
        },
      ),
      title: new Text('Edit File'),
    );
  }


  //upadating dialog
  Container updatingDialog(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),
        color: Colors.black54,
      ),
      height: screenAwareSize(100, context),
      width: screenAwareSize(200, context),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            child: Text('Uploading...', style: TextStyle(color: Colors.white),),
            padding: EdgeInsets.all(7),
          ),
          Center(child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          ),)
        ],
      ),
    );
  }

  //uploadField
  Container uploadField() {
    return Container(
      width: screenAwareSize(120, context),
      height: screenAwareSize(120, context),
      child: IconButton(icon: Icon(file != null ? Icons.insert_drive_file : Icons.cloud_upload, size: screenAwareSize(120, context), color: Colors.green,),onPressed: () async{
        await getFile();
      },),
    );
  }

  //File name
  TextFormField nameTextForm(BuildContext context) {
    return TextFormField(
      controller: _controller,
      decoration: new InputDecoration(
        border: new OutlineInputBorder(
          borderRadius: BorderRadius.circular(screenAwareSize(20, context)),
        ),
        contentPadding: EdgeInsets.all(0.0),
        labelText: 'Name',
        prefixIcon: const Icon(
          Icons.person,
        ),
      ),
      onSaved: (value) {
          _fileName = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          return 'Name can\'t be empty';
        }
      },
    );
  }


//  if there is no internet connection this dialog will pop up
  void _showInternetAlertDialouge( ) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: screenAwareSize(10, context)),
                Text('No Internet 😞',
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: screenAwareSize(26, context)),
                ),
                SizedBox(height: screenAwareSize(10, context)),
                Padding(
                  padding: EdgeInsets.all(screenAwareSize(8.0, context)),
                  child: Text(
                    'Please Check Internet Connection.',
                    style: TextStyle(
                        color: Colors.black87,
                        fontSize: screenAwareSize(16, context)),
                    textAlign: TextAlign.center,
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'CLOSE',
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.teal,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    elevation: 6.0,
                  ),
                )
              ],
            ),
            contentPadding: EdgeInsets.all(10),
            titlePadding: EdgeInsets.all(20),
          );
        });
  }

  //checking internet connection state
  Future<bool> checkInternetConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      return false;
    }
    return true;
  }
}
