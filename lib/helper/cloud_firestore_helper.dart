import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_constructible/model/user_model.dart';
import '../model/file_model.dart';
import '../model/project_model.dart';

class FireCloudStoreHelper {
  CollectionReference userReference = Firestore.instance.collection('/users');
  CollectionReference projectReference = Firestore.instance.collection('/projects');
  CollectionReference fileReference = Firestore.instance.collection('/files');

  //creating new user
  Future<bool> storeNewUser(UserModel model) async {
    var result = await userReference.add(model.toJson());
    if (result.documentID != null) {
      return true;
    } else {
      return false;
    }
  }

  //adding Project
  Future<bool> addProject(ProjectModel model) async {
    var result = await projectReference.add(model.toJson());
    if (result.documentID != null) {
      return true;
    } else {
      return false;
    }
  }

  // update Project
  Future<void> updateProject(ProjectModel model, documentID) async {
    var res = projectReference.document(documentID).updateData(model.toJson());
    return res;
  }

  //deleting project
  deleteProject(docId)  {
    projectReference.document(docId).delete().catchError((e) {
      print(e);
    });
  }

  //add new file
  Future<bool> addFile(FileModel model) async {
    var result = await fileReference.add(model.toJson());
    if (result.documentID != null) {
      return true;
    } else {
      return false;
    }
  }

  //deleting file
  deleteFile(docId) {
    fileReference.document(docId).delete().catchError((e) {
      print(e);
    });
  }

  // update file
  Future<void> updateFile(FileModel model, documentID) async {
    var res = fileReference.document(documentID).updateData(model.toJson());
    return res;
  }

}
