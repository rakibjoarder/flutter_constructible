import 'package:flutter/material.dart';
import 'package:flutter_constructible/pages/login/login_page.dart';
import 'package:flutter_constructible/pages/login/signup_page.dart';
import 'package:flutter_constructible/pages/page_routes.dart';
import 'package:flutter_constructible/pages/projects/project_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Demo',
        debugShowCheckedModeBanner: false,
        routes: <String, WidgetBuilder>{
          '/login': (BuildContext context) => new LoginPage(),
          '/signup': (BuildContext context) => new SignUpPage(),
          '/projectlist': (BuildContext context) => new ProjectList(),
        },
        theme: new ThemeData(
          primarySwatch: Colors.teal,
        ),
        home: PageRoutes());
  }
}
